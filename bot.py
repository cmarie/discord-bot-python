#!/usr/bin/env python3
from discord.ext import commands
from discord import Embed
import json

config = {}
with open('config.json', 'r') as f:
    config = json.loads(f.read())

bot = commands.Bot(command_prefix=config['command_prefix'], description="")


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.command(pass_context=True)
async def test(ctx):
    if ctx.message.server is not None:
        await bot.delete_message(ctx.message)
    await bot.whisper("Works")


@bot.command(pass_context=True)
async def test(ctx, string: str):
    if ctx.message.server is not None:
        await bot.delete_message(ctx.message)
    await bot.whisper(string)


@bot.command(pass_context=True)
async def whisper(ctx):
    if ctx.message.server is not None:
        await bot.delete_message(ctx.message)
    await bot.whisper("Works")


@bot.command(pass_context=True)
async def roles(ctx):
    if ctx.message.server is not None:
        await bot.delete_message(ctx.message)
        rc = Embed()
        for role in ctx.message.server.roles:
            rc.add_field(name=role.name, value=role.id)
        await bot.whisper(embed=rc)
    else:
        await bot.say("This command can only be run on a server")


@bot.command(pass_context=True)
async def channels(ctx):
    if ctx.message.server is not None:
        await bot.delete_message(ctx.message)
        rc = Embed()
        for channel in ctx.message.server.channels:
            if channel.bitrate is None:
                rc.add_field(name=channel.name, value=channel.id)
        await bot.whisper(embed=rc)
    else:
        await bot.say("This command can only be run on a server")


bot.run(config["bot-token"])
