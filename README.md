# Prerequisites
1. [Click Here](https://discordapp.com/developers/applications/me) to create an app, user and client id
2. [Click Here](https://discordapi.com/permissions.html) to calculate the permission number needed for your app
3. Copy your client id from step 1 and paste it into the `Insert Client ID Here` input box at the bottom of the page in step 2 and click the link it generates to register your bot on a server

# Installation (Linux)
1. `apt-get install python3 python3-pip`
2. `python3 -m pip install -U html5lib`
3. `python3 -m pip install -U discord.py`
4. `python3 -m pip install -U asyncio`
5. `python3 -m pip install -U aiohttp`
6. `python3 -m pip install -U websockets`
7. `python3 -m pip install -U requests`
8. `python3 -m pip install -U bs4`

# Installation (Windows)
1. https://www.python.org/ftp/python/3.6.2/python-3.6.2-amd64.exe
2. https://bootstrap.pypa.io/get-pip.py
3. `python get-pip.py`
4. `python -m pip install -U html5lib`
5. `python -m pip install -U discord.py`
6. `python -m pip install -U asyncio`
7. `python -m pip install -U aiohttp`
8. `python -m pip install -U websockets`
9. `python -m pip install -U requests`
10. `python -m pip install -U bs4`

Rename `config.json.example` to `config.json`

Modify `config.json` to suit your application

# Client Usage
`await client.send_message(discord.Object(id=CHANNEL_ID), "Some Message")`

`await client.send_message(message.server, "Some Message"")`
# Bot Usage
`await bot.say("Some Message")`

`await bot.whisper("Some Message")`

# Rich Content
``` Python
@bot.command(pass_context=True)
async def test(ctx):
    em = Embed()
    em.title = "Some Title"
    em.colour = 0x000000  #Black
    em.description = "Some Description"
    em.add_field(name="Field Name", value="Field Value")
    em.add_field(name="Field Name", value="Field Value")
    em.add_field(name="Field Name", value="Field Value")
    em.add_field(name="Field Name", value="Field Value")
    em.set_footer(text="Some Footer Text", icon_url="URL to some icon (Optional)")
    em.set_thumbnail(url="URL to some thumbnail")
    
    await bot.say(embed=em)
```