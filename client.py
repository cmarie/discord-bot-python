#!/usr/bin/env python3
import asyncio
import json
import discord
import datetime
from typing import List
from discord import Embed
from discord import Reaction
from discord import User
from discord import Member
from discord import Message
from discord import Channel
from discord import Server
from discord import Emoji
from discord import Role

config = {}
with open('config.json', 'r') as f:
    config = json.loads(f.read())

client = discord.Client()


# Reaction Events
@client.event
async def on_reaction_add(reaction: Reaction, user: User):
    pass  # Remove


@client.event
async def on_reaction_remove(reaction: Reaction, user: User):
    pass  # Remove


@client.event
async def on_reaction_clear(message: Message, reactions: List[Reaction]):
    pass  # Remove


@client.event
async def on_reaction_clear(message: Message, reactions: List[Reaction]):
    pass  # Remove


# Channel Events
@client.event
async def on_channel_create(channel: Channel):
    pass  # Remove


@client.event
async def on_channel_update(before: Channel, after: Channel):
    pass  # Remove


@client.event
async def on_channel_delete(channel: Channel):
    pass  # Remove


# Message Events
@client.event
async def on_message_edit(before: Message, after: Message):
    pass  # Remove


@client.event
async def on_message(message: Message):
    pass  # Remove


@client.event
async def on_message_delete(message: Message):
    pass  # Remove


# Member Events
@client.event
async def on_member_join(member: Member):
    pass  # Remove


@client.event
async def on_member_remove(member: Member):
    pass  # Remove


@client.event
async def on_member_update(before: Member, after: Member):
    pass  # Remove


@client.event
async def on_member_ban(member: Member):
    pass  # Remove


@client.event
async def on_member_unban(server: Server, user: User):
    pass  # Remove


# Typing Events
@client.event
async def on_typing(channel: Channel, user: User, when: datetime.datetime):
    pass  # Remove


# Group Events
@client.event
async def on_group_join(channel: Channel, user: User):
    pass  # Remove


@client.event
async def on_group_remove(channel: Channel, user: User):
    pass  # Remove


# Socket Events
@client.event
async def on_socket_raw_receive(msg):
    pass  # Remove


@client.event
async def on_socket_raw_send(payload):
    pass  # Remove


# Client Events
@client.event
async def on_resumed():
    pass  # Remove


@client.event
async def on_ready():
    pass  # Remove


@client.event
async def on_error(event, *args, **kwargs):
    pass  # Remove


# Server Events
@client.event
async def on_server_join(server: Server):
    pass  # Remove


@client.event
async def on_server_remove(server: Server):
    pass  # Remove


@client.event
async def on_server_update(before: Server, after: Server):
    pass  # Remove


@client.event
async def on_server_role_create(role: Role):
    pass  # Remove


@client.event
async def on_server_role_update(before: Role, after: Role):
    pass  # Remove


@client.event
async def on_server_emojis_update(before: Emoji, after: Emoji):
    pass  # Remove


@client.event
async def on_server_available(server: Server):
    pass  # Remove


client.run(config["client_token"])
